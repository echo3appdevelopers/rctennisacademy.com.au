<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;

class Products extends Model
{
    use SortableTrait, Sortable;

    protected $table = 'products';

    public $sortable = ['title', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    public function categorysort()
    {
        return $this->hasOne(ProductCategory::class,'id','category_id');
    }

	public function images()
    {
        return $this->hasMany(ProductImage::class, 'product_id')->orderBy('position', 'asc');
    }
	
    public function scopeFilter($query)
    {

        $filter = session()->get('products-filter');
        $select = "";

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);			
        }

        if($filter['search']){
            $select =  $query
				            ->where('title','like', '%'.$filter['search'].'%');				           
				            ;
        }

        return $select;
    }
}
