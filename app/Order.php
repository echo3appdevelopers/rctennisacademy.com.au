<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;

class Order extends Model
{
    use SortableTrait, Sortable;

    protected $table = 'orders';

    public $sortable = ['number', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('deleted_at','=', null);
    }  

    public function scopeFilter($query)
    {

        $filter = session()->get('orders-filter');
        $select = "";
       
        if($filter['search']){
            $select =  $query->where('number','like', '%'.$filter['search'].'%');
        }

        return $select;
    }
	
	public function billpayerssort()
    {
        return $this->hasOne(Billpayer::class,'id','billpayer_id');
    }
	
	public function billpayers()
    {
        return $this->hasOne(Billpayer::class,'id', 'billpayer_id')->with("addresses");
    }
	
	public function shiptoaddresses()
    {
        return $this->hasOne(Address::class,'id', 'shipping_address_id');
    }
	
	public function items()
    {
        return $this->hasMany(OrderItem::class, 'order_id')->orderBy('id', 'asc');
    }
	
	
	
}
