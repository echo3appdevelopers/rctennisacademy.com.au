<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Kyslik\ColumnSortable\Sortable;
//use Rutorika\Sortable\SortableTrait;

class ProjectImage extends Model
{
    //use SortableTrait, Sortable;

    protected $table = 'project_images';

    //public $sortable = ['title', 'category_id', 'status'];  
}
