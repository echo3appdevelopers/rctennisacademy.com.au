<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Testimonial;
use App\Helpers\General;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class TestimonialsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $testimonials = Testimonial::Filter()->sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        } else {
            $testimonials = Testimonial::sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('testimonials-filter');       
        return view('admin/testimonials/testimonials', array(
            'testimonials' => $testimonials,           
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {        
        return view('admin/testimonials/add', array(          
        ));
    }

    public function edit($testimonial_id)
    {
        $testimonial = Testimonial::where('id', '=', $testimonial_id)->first();       
        return view('admin/testimonials/edit', array(
            'testimonial' => $testimonial,           
        ));
    }

	public function preview($testimonial_id)
    {
		$testimonial = Testimonial::where('id', '=', $testimonial_id)->first();		
		
		$general = new General();
		$view = $general->testimonialPreview($testimonial->slug);	
		
        return ($view);
    }
	
    public function store(Request $request)
    {
        $rules = array(            
            'person' => 'required',
            'description' => 'required'
        );

        $messages = [            
            'person.required' => 'Please enter person/company',
            'description.required' => 'Please enter desciption'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/testimonials/add')->withErrors($validator)->withInput();
        }

        $testimonial = new Testimonial();        
        $testimonial->person = $request->person;
        $testimonial->description = $request->description;
        $testimonial->image = $request->image;
		
        if($request->live=='on'){
           $testimonial->status = 'active'; 
        }

        $testimonial->save();
   
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/testimonials/' . $testimonial->id . '/edit')->with('message', Array('text' => 'Testimonial has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/testimonials/')->with('message', Array('text' => 'Testimonial has been added', 'status' => 'success'));
		}		        


    }

    public function update(Request $request)
    {
        $rules = array(          
            'person' => 'required',
            'description' => 'required'
        );

        $messages = [           
            'person.required' => 'Please enter title',
            'description.required' => 'Please enter desciption'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/testimonials/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $testimonial = Testimonial::where('id','=',$request->id)->first();       
        $testimonial->person = $request->person;
        $testimonial->description = $request->description;
		$testimonial->image = $request->image;
		if($request->live=='on'){
           $testimonial->status = 'active'; 
		} else {
			$testimonial->status = 'passive';
        }
        $testimonial->save();
    
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/testimonials/' . $testimonial->id . '/edit')->with('message', Array('text' => 'Testimonial has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/testimonials/')->with('message', Array('text' => 'Testimonial has been updated', 'status' => 'success'));
		}
    }

    public function delete($testimonial_id)
    {
        $testimonial = Testimonial::where('id','=',$testimonial_id)->first();
        $testimonial->is_deleted = true;
        $testimonial->save();

        return \Redirect::back()->with('message', Array('text' => 'Testimonial has been deleted.', 'status' => 'success'));
    }

    public function changeTestimonialStatus(Request $request, $testimonial_id)
    {
        $testimonial = Testimonial::where('id', '=', $testimonial_id)->first();
        if ($request->status == "true") {
            $testimonial->status = 'active';
        } else if ($request->status == "false") {
            $testimonial->status = 'passive';
        }
        $testimonial->save();

        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $testimonials = Testimonial::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/testimonials/sort', array(
            'testimonials' => $testimonials
        ));
    }
    
    public function emptyFilter()
    {
        session()->forget('testimonials-filter');
        return redirect()->to('dreamcms/testimonials');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('testimonials-filter', [                
                'search' => $request->search
            ]);
        }

        if (session()->has('testimonials-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }

}