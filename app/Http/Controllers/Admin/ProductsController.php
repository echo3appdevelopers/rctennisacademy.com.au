<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Products;
use App\ProductCategory;
use App\ProductImage;
use App\Helpers\General;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {				
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $products = Products::Filter()->sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        } else {
            $products = Products::with('category')->sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('products-filter');
        $categories = ProductCategory::orderBy('created_at', 'desc')->get();
        return view('admin/products/products', array(
            'products' => $products,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $categories = ProductCategory::orderBy('created_at', 'desc')->get();
        $images = array();

        if(old('imageCount')){

            for($i=0; $i<old('imageCount'); $i++){

                $properties = new \stdClass();
                $properties->id = old('imgId'.$i);
                $properties->position = old('imgPosition'.$i);
                $properties->delete = old('imgDelete'.$i);
                $properties->status = old('imgStatus'.$i);
                $properties->location = old('imgLocation'.$i);
                $properties->type = old('imgType'.$i);

                $images[$i] = $properties;
            }

            $images = collect($images)->sortBy('position');
        }

        return view('admin/products/add', array(
            'categories' => $categories,
            'images' => $images
        ));
    }

    public function edit($product_id)
    {
        $product = Products::where('id', '=', $product_id)->with("images")->first();
        $categories = ProductCategory::orderBy('created_at', 'desc')->get();

        if(old('imageCount')){

            for($i=0; $i<old('imageCount'); $i++){

                $properties = new \stdClass();
                $properties->id = old('imgId'.$i);
                $properties->position = old('imgPosition'.$i);
                $properties->delete = old('imgDelete'.$i);
                $properties->status = old('imgStatus'.$i);
                $properties->location = old('imgLocation'.$i);
                $properties->type = old('imgType'.$i);

                $images[$i] = $properties;
            }

            $product->images = collect($images)->sortBy('position');
        }

        return view('admin/products/edit', array(
            'product' => $product,
            'categories' => $categories
        ));
    }

    public function store(Request $request)
    {
        $rules = array(            
            'title' => 'required',
            'slug' => 'required|unique_store:products',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'short_description' => 'required',	
			'description' => 'required',	
			'price' => 'required',	
			'state' => 'required',	
        );

        $messages = [
            'title.required' => 'Please select title',
			'slug.required' => 'Please enter unique SEO Name',
			'slug.unique_store' => 'The SEO Name is already taken',
            'meta_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
            'short_description.required' => 'Please enter short desciption',
			'description.required' => 'Please enter desciption',
			'price.required' => 'Please enter price',
			'state.required' => 'Please enter status',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/products/add')->withErrors($validator)->withInput();
        }

        $product = new Products();
        $product->category_id = $request->category_id;
        $product->name = $request->title;
		$product->slug = $request->slug; 
		$product->sku = $request->sku;
        $product->ext_title = $request->meta_title;
        $product->meta_keywords = $request->meta_keywords;
        $product->meta_description = $request->meta_description;
        $product->excerpt = $request->short_description;
		$product->description = $request->description;
		$product->price = $request->price;        
        $product->state = $request->state;        

        $product->save();
        $this->storeImages($request, $product->id);
		
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/products/' . $product->id . '/edit')->with('message', Array('text' => 'Product has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/products')->with('message', Array('text' => 'Product has been added', 'status' => 'success'));
		}
    }

	public function storeImages(Request $request, $project_id)
    {

		$deleteCount = 0;
		
		for($i = 0; $i < $request->imageCount; $i++)  {
			if (isset($request["imgDelete" . $i]) && $request["imgDelete" . $i] == 1)  {

                if($request["imgType" . $i]!="new"){
                    // Delete Image
                    $product_image = ProductImage::where('id','=',$request["imgId" . $i])->first();
                    $project_product_imageimage->delete();
                    $deleteCount++;
                }

			} else  {
				// Save Image (New or Update)
                if($request["imgType" . $i]=="new"){
				   $image = new ProductImage();
				} else  {
				   $image = ProductImage::where('id','=',$request["imgId" . $i])->first();	
				}

				$image->product_id = $project_id;
				$image->location = $request["imgLocation" . $i]; 
				$image->position = $request["imgPosition" . $i] - $deleteCount; 
				if($request["imgStatus" . $i]=='on'){
				   $image->status = 'active'; 
				} else {
				   $image->status = 'passive'; 	
				}

				$image->save();
			}
		}
    }
	
    public function update(Request $request)
    {
        $rules = array(
            'name' => 'required',
			'slug' => 'required|unique_update:projects,' . $request->id,
            'ext_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'excerpt' => 'required',            
            'description' => 'required',
			'price' => 'required',
			'state' => 'required',
        );

        $messages = [
            'name.required' => 'Please enter title',
			'slug.required' => 'Please enter unique SEO Name',
			'slug.unique_update' => 'The SEO Name is already taken',
            'ext_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
            'excerpt.required' => 'Please enter short desciption',           
            'description.required' => 'Please enter description',
			'price.required' => 'Please enter price',
			'state.required' => 'Please enter status'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            //return redirect('dreamcms/products/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $product = Products::where('id','=',$request->id)->first();
        $product->category_id = $request->category_id;
        $product->name = $request->name;
        $product->slug = $request->slug;
		$product->sku = $request->sku;
        $product->ext_title = $request->ext_title;
        $product->meta_keywords = $request->meta_keywords;
        $product->meta_description = $request->meta_description;
        $product->excerpt = $request->excerpt;        
        $product->description = $request->description;        
		$product->price = $request->price;        		
        $product->state = $request->state;        	
       
        $product->save();
        $this->storeImages($request, $product->id);
		
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/products/' . $product->id . '/edit')->with('message', Array('text' => 'Product has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/products')->with('message', Array('text' => 'Product has been updated', 'status' => 'success'));
		}
		
        
    }

    public function delete($product_id)
    {
        $product = Products::where('id','=',$product_id)->first();
        $product->is_deleted = true;
        $product->save();

        return \Redirect::back()->with('message', Array('text' => 'Product has been deleted.', 'status' => 'success'));
    }

    public function changepProductStatus(Request $request, $product_id)
    {
        $product = Products::where('id', '=', $product_id)->first();
        if ($request->status == "true") {
            $product->status = 'active';
        } else if ($request->status == "false") {
            $product->status = 'passive';
        }
        $product->save();

        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $products = Products::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/products/sort', array(
            'products' => $products
        ));
    }

    public function categories(Request $request)
    {		       		
        $categories = ProductCategory::orderBy('position', 'desc')->get();
        return view('admin/products/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/products/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
			'slug' => 'required|unique_store:product_categories'
        );

        $messages = [
            'name.required' => 'Please enter name.',
			'slug.unique_store' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/products/add-category')->withErrors($validator)->withInput();
        }

        $category = new ProductCategory();
        $category->name = $request->name;		
		$category->slug = $request->slug;
		$category->description = $request->description;
		if($request->live=='on'){
           $category->status = 'active'; 
        }
        $category->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/products/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/products/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		}		        

    }

    public function editCategory($category_id)
    {
        $category = ProductCategory::where('id', '=', $category_id)->first();
        return view('admin/products/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',	
			'slug' => 'required|unique_update:project_categories,'.$request->id
        );

        $messages = [
            'name.required' => 'Please enter name.',
			'slug.unique_update' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/products/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = ProductCategory::findOrFail($request->id);
        $category->name = $request->name;	
		$category->slug = $request->slug;
		$category->description = $request->description;
		if($request->live=='on'){
           $category->status = 'active'; 
		} else {
			$category->status = 'passive';
        }
        $category->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/products/' . $category->id . '/edit-type')->with('message', Array('text' => 'Category has been update', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/products/categories')->with('message', Array('text' => 'Category has been update', 'status' => 'success'));
		}	
		        
    }

    public function deleteCategory($category_id)
    {
        $category = ProductCategory::where('id','=',$category_id)->first();

        if(count($category->products)){
            return \Redirect::to('dreamcms/products/categories')->with('message', Array('text' => 'Category has products. Please delete products first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        return \Redirect::to('dreamcms/products/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function emptyFilter()
    {
        session()->forget('products-filter');
        return redirect()->to('dreamcms/products');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->type && $request->type != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('products-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('products-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }
	
	public function changeCategoryStatus(Request $request, $category_id)
    {
        $category = ProductCategory::where('id', '=', $category_id)->first();
        if ($request->status == "true") {
            $category->status = 'active';
        } else if ($request->status == "false") {
            $category->status = 'passive';
        }
        $category->save();

        return Response::json(['status' => 'success']);
    }

    public function sortCategory()
    {
        $categories = ProductCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/products/sort-category', array(
            'categories' => $categories
        ));
    }
	
	public function preview($product_id)
    {
		$product = Products::with("category")->where('id', '=', $product_id)->first();		
		
		$general = new General();
		$view = $general->productPreview($product->category->slug, $product->slug);	
		
        return ($view);
    }

}