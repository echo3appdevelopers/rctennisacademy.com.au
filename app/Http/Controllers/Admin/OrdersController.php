<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use App\State;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $orders = Order::Filter()->with("billpayers")->sortable()->orderBy('id', 'desc')->paginate($paginate_count);
        } else {
            $orders = Order::sortable()->with("billpayers")->orderBy('id', 'desc')->paginate($paginate_count);
        }
		
        $session = session()->get('orders-filter');
        
        return view('admin/orders/orders', array(
            'orders' => $orders,            
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }
  
    public function edit($order_id)
    {
        $order = Order::where('id', '=', $order_id)->with("billpayers")->with("shiptoaddresses")->with("items")->first();
        
        return view('admin/orders/edit', array(
            'order' => $order,
			'states' => State::all()
        ));
    }		    

    public function update(Request $request)
    {
        $rules = array(            
            'status' => 'required', 
			'paid_status' => 'required', 
        );

        $messages = [           
            'status.required' => 'Please enter status',  
			'paid_status.required' => 'Please enter payment status',  
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/orders/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $order = Order::where('id','=',$request->id)->first();       
        $order->status = $request->status;
	    $order->paid_status = $request->paid_status;
        $order->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/orders/' . $order->id . '/edit')->with('message', Array('text' => 'Order has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/orders/')->with('message', Array('text' => 'Order has been updated', 'status' => 'success'));
		}		       
    }

    public function delete($order_id)
    {
        $order = Order::where('id','=',$order_id)->first();
        $order->deleted_at = date('Y-m-d H:i:s');
        $order->save();

        return \Redirect::back()->with('message', Array('text' => 'Order has been deleted.', 'status' => 'success'));
    }

    public function changeOrderStatus(Request $request, $order_id)
    {
        $order = Order::where('id', '=', $order_id)->first();
        if ($request->status == "true") {
            $order->status = 'active';
        } else if ($request->status == "false") {
            $order->status = 'passive';
        }
        $order->save();

        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $documents = Order::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/orders/sort', array(
            'orders' => $orders
        ));
    }
    
    public function emptyFilter()
    {
        session()->forget('orders-filter');
        return redirect()->to('dreamcms/orders');
    }

    public function isFiltered($request)
    {

        $filter_control = false;
        
        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('orders-filter', [                
                'search' => $request->search
            ]);
        }

        if (session()->has('orders-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }
}