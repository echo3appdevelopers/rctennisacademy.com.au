<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Member;
use App\MemberType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class MembersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $members = Member::Filter()->sortable()->orderBy('id', 'desc')->paginate($paginate_count);
			//$members = Member::Filter()->orderBy('id', 'desc')->paginate($paginate_count);
        } else {
            $members = Member::with('type')->sortable()->orderBy('id', 'desc')->paginate($paginate_count);
			//$members = Member::with('type')->orderBy('id', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('Members-filter');
        $types = MemberType::orderBy('created_at', 'desc')->get();
        return view('admin/members/members', array(
            'members' => $members,
            'types' => $types,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $types = MemberType::orderBy('created_at', 'desc')->get();
        return view('admin/members/add', array(
            'types' => $types
        ));
    }

    public function edit($member_id)
    {
        $member = Member::where('id', '=', $member_id)->first();
        $types = MemberType::orderBy('created_at', 'desc')->get();
        return view('admin/members/edit', array(
            'member' => $member,
            'types' => $types
        ));
    }

    public function store(Request $request)
    {
        $rules = array(            
            'title' => 'required',
            'firstName' => 'required',
			'lastName' => 'required',
			'address1' => 'required',
			'suburb' => 'required',
			'state' => 'required',
			'postcode' => 'required',
			'email' => 'required|string|email|max:255|unique_store:members',
			'type_id' => 'required',
			'dateJoin' => 'required',
			'dateExpire' => 'required',
			'password' => 'required',
        );

        $messages = [
            'title.required' => 'Please select title',
			'firstName.required' => 'Please enter first name',
			'lastName.required' => 'Please enter last name',
			'address1.required' => 'Please enter address line 1',
			'suburb.required' => 'Please enter suburb',
			'state.required' => 'Please enter state',
			'postcode.required' => 'Please enter postcode',
			'email.required' => 'Please enter email',
			'email.unique_store' => 'The email has already been taken.',
			'type_id.required' => 'Please select type',
			'dateJoin.required' => 'Please enter join date',
			'dateExpire.required' => 'Please enter expiry date',
			'password.required' => 'Please enter password',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/members/add')->withErrors($validator)->withInput();
        }

        $member = new Member();
        $member->type_id = $request->type_id;
        $member->title = $request->title;
        $member->firstName = $request->firstName;
		$member->lastName = $request->lastName;
		$member->occupation = $request->occupation;
		$member->companyName = $request->companyName;
		$member->address1 = $request->address1;
		$member->address2 = $request->address2;
		$member->suburb = $request->suburb;
		$member->state = $request->state;
		$member->postcode = $request->postcode;
		$member->phoneMobile = $request->phoneMobile;
		$member->phoneLandline = $request->phoneLandline;
		$member->email = $request->email;
		$member->dateJoin = date('Y-m-d' , strtotime(str_replace("/", "-", $request->dateJoin)));
		$member->dateExpire = date('Y-m-d' , strtotime(str_replace("/", "-", $request->dateExpire)));
		$member->password = Hash::make($request->password);
        if($request->newsletters=='on'){
           $member->newsletters = 'active'; 
        }
        if($request->live=='on'){
           $member->status = 'active'; 
        }

        $member->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/members/' . $member->id . '/edit')->with('message', Array('text' => 'Member has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/members')->with('message', Array('text' => 'Member has been added', 'status' => 'success'));
		}
    }

    public function update(Request $request)
    {
        $rules = array(
            'title' => 'required',
            'firstName' => 'required',
			'lastName' => 'required',
			'address1' => 'required',
			'suburb' => 'required',
			'state' => 'required',
			'postcode' => 'required',
			'email' => 'required|string|email|max:255|unique_update:members,'.$request->id,
			'type_id' => 'required',
			'dateJoin' => 'required',
			'dateExpire' => 'required',
        );

        $messages = [
            'title.required' => 'Please select title',
			'firstName.required' => 'Please enter first name',
			'lastName.required' => 'Please enter last name',
			'address1.required' => 'Please enter address line 1',
			'suburb.required' => 'Please enter suburb',
			'state.required' => 'Please enter state',
			'postcode.required' => 'Please enter postcode',
			'email.required' => 'Please enter email',
            'email.unique_update' => 'The email has already been taken.',
			'type_id.required' => 'Please select type',            
			'dateJoin.required' => 'Please enter join date',
			'dateExpire.required' => 'Please enter expiry date',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/members/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $member = Member::where('id','=',$request->id)->first();
        $member->type_id = $request->type_id;
        $member->title = $request->title;
        $member->firstName = $request->firstName;
		$member->lastName = $request->lastName;
		$member->occupation = $request->occupation;
        $member->companyName = $request->companyName;
		$member->address1 = $request->address1;
		$member->address2 = $request->address2;
		$member->suburb = $request->suburb;
		$member->state = $request->state;
		$member->postcode = $request->postcode;
		$member->phoneMobile = $request->phoneMobile;
		$member->phoneLandline = $request->phoneLandline;
		$member->email = $request->email;
		$member->dateJoin = date('Y-m-d' , strtotime(str_replace("/", "-", $request->dateJoin)));
		$member->dateExpire = date('Y-m-d' , strtotime(str_replace("/", "-", $request->dateExpire)));
		if ($request->password != "")  {
		   $member->password = Hash::make($request->password);
		}
        if($request->newsletters=='on'){
           $member->newsletters = 'active';
		} else {
			$member->newsletters = 'passive';
        }
        if($request->live=='on'){
           $member->status = 'active'; 
		} else {
			$member->status = 'passive';
        }
        $member->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/members/' . $member->id . '/edit')->with('message', Array('text' => 'Member has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/members')->with('message', Array('text' => 'Member has been updated', 'status' => 'success'));
		}
		
        
    }

    public function delete($member_id)
    {
        $member = Member::where('id','=',$member_id)->first();
        $member->is_deleted = true;
        $member->save();

        return \Redirect::back()->with('message', Array('text' => 'Member has been deleted.', 'status' => 'success'));
    }

    public function changeMemberStatus(Request $request, $member_id)
    {
        $member = Member::where('id', '=', $member_id)->first();
        if ($request->status == "true") {
            $member->status = 'active';
        } else if ($request->status == "false") {
            $member->status = 'passive';
        }
        $member->save();

        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $members = Member::where('status','=','active')->orderBy('id', 'desc')->get();

        return view('admin/members/sort', array(
            'members' => $members
        ));
    }

    public function types()
    {		       		
        $types = MemberType::orderBy('position', 'desc')->get();
        return view('admin/members/types', compact('types'));
    }

    public function addType()
    {
        return view('admin/members/add-type');
    }

    public function storeType(Request $request)
    {
        $rules = array(
            'name' => 'required',
			'price' => 'required'
        );

        $messages = [
            'name.required' => 'Please enter name.',
			'price.required' => 'Please enter price.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/members/add-type')->withErrors($validator)->withInput();
        }

        $type = new MemberType();
        $type->name = $request->name;
		$type->price = str_replace(',', '', $request->price);
		if($request->live=='on'){
           $type->status = 'active'; 
        }
        $type->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/members/' . $type->id . '/edit-type')->with('message', Array('text' => 'Type has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/members/types')->with('message', Array('text' => 'Type has been added', 'status' => 'success'));
		}		        

    }

    public function editType($type_id)
    {
        $type = MemberType::where('id', '=', $type_id)->first();
        return view('admin/members/edit-type', array(
            'type' => $type,
        ));
    }

    public function updateType(Request $request)
    {
        $rules = array(
            'name' => 'required',
			'price' => 'required'
        );

        $messages = [
            'name.required' => 'Please enter name.',
			'price.required' => 'Please enter price.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/members/' . $request->id . '/edit-type')->withErrors($validator)->withInput();
        }

        $price = str_replace('.', '', $request->price);
        $price = str_replace(',', '.', $price);

        $type = MemberType::findOrFail($request->id);
        $type->name = $request->name;
		$type->price = str_replace(',', '', $request->price);
		if($request->live=='on'){
           $type->status = 'active'; 
		} else {
			$type->status = 'passive';
        }
        $type->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/members/' . $type->id . '/edit-type')->with('message', Array('text' => 'Type has been update', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/members/types')->with('message', Array('text' => 'Type has been update', 'status' => 'success'));
		}	
		        
    }

    public function deleteType($type_id)
    {
        $type = MemberType::where('id','=',$type_id)->first();

        if(count($type->members)){
            return \Redirect::to('dreamcms/members/types')->with('message', Array('text' => 'Member Type has members. Please delete members first.', 'status' => 'error'));
        }

        $type->is_deleted = true;
        $type->save();

        return \Redirect::to('dreamcms/members/types')->with('message', Array('text' => 'Member Type has been deleted.', 'status' => 'success'));
    }

    public function emptyFilter()
    {
        session()->forget('members-filter');
        return redirect()->to('dreamcms/members');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->type && $request->type != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('members-filter', [
                'type' => $request->type,
                'search' => $request->search
            ]);
        }

        if (session()->has('members-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }
	
	public function changeTypeStatus(Request $request, $type_id)
    {
        $type = MemberType::where('id', '=', $type_id)->first();
        if ($request->status == "true") {
            $type->status = 'active';
        } else if ($request->status == "false") {
            $type->status = 'passive';
        }
        $type->save();

        return Response::json(['status' => 'success']);
    }

    public function sortType()
    {
        $types = MemberType::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/members/sort-type', array(
            'types' => $types
        ));
    }

}