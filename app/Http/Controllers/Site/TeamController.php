<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TeamMember;
use App\TeamCategory;

class TeamController extends Controller
{
    public function index(Request $request)
    {

        $categories = TeamCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        if($request->category==''){
            $selected_category = TeamCategory::where('status','=','active')->whereHas('members')->orderBy('position', 'desc')->first();
        }else{
            $selected_category = TeamCategory::where('slug','=',$request->category)->first();
        }

        $items = null;
        $meta_title_inner = "Team";
        $meta_keywords_inner = "Team";
        $meta_description_inner = "Team";

        if($selected_category){
            $items = TeamMember::where('category_id','=', $selected_category->id)->paginate(10);

            $meta_title_inner = $selected_category->name. " - Team";
            $meta_description_inner = $selected_category->name . " - Team";
        }

        return view('site/team/list', array(
            'categories' => $categories,
            'category' => $selected_category,
            'items' => $items,
            'meta_title_inner' => $meta_title_inner,
            'meta_keywords_inner' => $meta_keywords_inner,
            'meta_description_inner' => $meta_description_inner
        ));

    }

    public function member($category, $team_member)
    {
        $categories = TeamCategory::where('status','=','active')->orderBy('position', 'desc')->get();
        $category = TeamCategory::where('slug','=',$category)->first();
        $member = TeamMember::where('slug','=',$team_member)->first();

        return view('site/team/item', array(
            'categories' => $categories,
            'category' => $category,
            'team_member' => $member,
            'meta_title_inner' => $member->name.'- Team',
            'meta_keywords_inner' => $member->name,
            'meta_description_inner' => $member->name
        ));
    }
}
