<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Project;
use App\ProjectCategory;

class ProjectsController extends Controller
{
    public function list(Request $request, $category_slug = "", $item_slug = ""){        		
    	$side_nav = $this->getCategories();	
		
		if (sizeof($side_nav) > 0)  {
			if ($category_slug == "")  {
			   // Get Latest Projects
			   $category_name = "Latest Projects";	

			   $items = $this->getProjects();
			} elseif ($category_slug != "" && $item_slug == "") {
			  // Get Category Projects	
			  $category = $this->getCategory($category_slug);
			  $category_name = $category->name;	

			  $items = $this->getProjects($category->id);			  
			} 		
		}
		
		return view('site/projects/list', array(            			
			'side_nav' => $side_nav,
			'category_name' => (sizeof($side_nav) > 0 ? $category_name : null),
			'items' => (sizeof($side_nav) > 0 ? $items : null),			
        ));

    }
	
    public function item(Request $request, $category_slug, $item_slug){
    	$side_nav = $this->getCategories();				  			
		$project_item = $this->getProjectItem($item_slug);			  
			
		return view('site/projects/item', array(            			
			'side_nav' => $side_nav,					
			'project_item' => $project_item,			
        ));
    }		
	public function getCategories(){
		$categories = ProjectCategory::where('status', '=', 'active')->get();		
		return($categories);
	}	
	
	public function getProjects($category_id = "", $limit = 2){
		if ($category_id == "")  {
			$projects = Project::where('status', '=', 'active')						
						->orderBy('position', 'desc')
						->paginate($limit);	
		} else {
		   $projects = Project::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)						
						->orderBy('position', 'desc')
						->paginate($limit);		
		}
		
		return($projects);
	}	
	  
	public function getProjectItem($item_slug){		
		$project = Project::where(['status' => 'active', 'slug' => $item_slug])
			        ->where('status', '=', 'active')
			        ->first();						
		return($project);
	}
	
	public function getCategory($category_slug){
		$categories = ProjectCategory::where('slug', '=', $category_slug)->first();		
		return($categories);
	}		
}
