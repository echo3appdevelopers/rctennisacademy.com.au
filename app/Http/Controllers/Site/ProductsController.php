<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Vanilo\Product\Contracts\Product;
use Vanilo\Product\Models\ProductProxy;

use App\Products;
use App\ProductCategory;

class ProductsController extends Controller
{
	
	public function list(Request $request, $category_slug = "", $item_slug = ""){        		
    	$side_nav = $this->getCategories();	
		
		if (sizeof($side_nav) > 0)  {
			if ($category_slug == "")  {
			   // Get Latest Products
			   $category_name = "Shop";	

			   $items = $this->getProducts();
			} elseif ($category_slug != "" && $item_slug == "") {
			  // Get Category Products	
			  $category = $this->getCategory($category_slug);
			  $category_name = $category->name;	

			  $items = $this->getProducts($category->id);			  
			} 		
		}
		
		return view('site/products/list', array(            			
			'side_nav' => $side_nav,
			'category_name' => (sizeof($side_nav) > 0 ? $category_name : null),
			'items' => (sizeof($side_nav) > 0 ? $items : null),	
			'page_type' => "Product",
        ));

    }
	
	public function item ($category_slug, $item_slug = "", $mode = "")
    {        
		$side_nav = $this->getCategories();		
		$product_item = $this->getProxyProductItem($item_slug, $mode);
		$display_product_item = $this->getProductItem($item_slug, $mode);			  
		
		return view('site/products/item', array(            			
			'side_nav' => $side_nav,					
			'product_item' => $product_item,	
			'display_product_item' => $display_product_item,	
			'mode' => $mode,
			'page_type' => "Product",
        ));
    }
		
	public function getCategories(){
		$categories = ProductCategory::whereHas("products")->where('status', '=', 'active')->get();		
		return($categories);
	}	
	
	public function getProducts($category_id = "", $limit = 12){
		if ($category_id == "")  {			
			$products = Products::where('state', '=', 3)						
						->orderBy('position', 'desc')
						->paginate($limit);	
		} else {		    
		   $products = Products::where('state', '=', 3)
			            ->where('category_id', '=', $category_id)						
						->orderBy('position', 'desc')
						->paginate($limit);		
		}
		
		return($products);
	}	
	
	public function getProxyProductItem($item_slug, $mode){		
		$product = ProductProxy::actives()->where(['slug' => $item_slug])->first();
		
		return($product);
	}
	
	public function getProductItem($item_slug, $mode){				
		if ($mode == "preview") {
		   $product = Products::where(['slug' => $item_slug])->first();							
		} else {			
		   $product = Products::where(['state' => 'active', 'slug' => $item_slug])->first();	
		}
		
		return($product);
	}
	
	public function getCategory($category_slug){
		$categories = ProductCategory::where('slug', '=', $category_slug)->first();		
		return($categories);
	}    
}
