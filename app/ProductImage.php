<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Kyslik\ColumnSortable\Sortable;
//use Rutorika\Sortable\SortableTrait;

class ProductImage extends Model
{
    //use SortableTrait, Sortable;

    protected $table = 'product_images';

    //public $sortable = ['title', 'category_id', 'status'];  
}
