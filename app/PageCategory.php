<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageCategory extends Model
{
    protected $table = 'page_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function pages()
    {
        return $this->hasMany(Page::class, 'category_id')->where('status', '=', 'active');
    }
	
	public function pagesnotsub()
    {
        return $this->hasMany(Page::class, 'category_id')->where('parent_page_id', '=', 'NULL')->where('status', '=', 'active');
    }
}
