<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;
use Spatie\Permission\Models\Permission;

class Module extends Model
{
    use SortableTrait, Sortable;

    protected $table = 'modules';

    public function permissions()
    {
        return $this->hasMany(Permission::class, 'module_id');
    }
}
