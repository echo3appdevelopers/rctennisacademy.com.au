<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyNewsTableChangeDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('news', function (Blueprint $table) {
           $table->dropColumn('start_date');
		   $table->dropColumn('archive_date');
		});
		Schema::table('news', function (Blueprint $table) {
			$table->date('start_date')->after('status');
			$table->date('archive_date')->after('start_date');
		});  		       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
