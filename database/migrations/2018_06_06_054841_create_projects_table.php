<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->integer('category_id');
            $table->string('title');
            $table->string('slug');
			$table->text('meta_title');
            $table->text('meta_keywords');
            $table->text('meta_description');
			$table->text('short_description')->nullable();
			$table->text('description')->nullable();		
			$table->enum('feature', ['active','passive'])->default('passive');
            $table->enum('status', ['active','passive'])->default('passive');   
			$table->integer('position');
            $table->timestamps();		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
