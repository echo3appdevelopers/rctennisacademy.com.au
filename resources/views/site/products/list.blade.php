<?php 
   // Set Meta Tags
   $meta_title_inner = ($category_name == "Latest Products" ? $category_name : $category_name . " - Products"); 
   $meta_keywords_inner = "Products"; 
   $meta_description_inner = ($category_name == "Latest Products" ? $category_name : $category_name . " - Products");  
?>

@extends('site/layouts/app')
@include('site/partials/carousel-inner')

@section('content')

<div class="blog-masthead ">
  <div class="container">            
      <div class="row">
        @include('site/partials/sidebar-products')
        
        <div class="col-sm-8 blog-main">
            <div class="blog-post">           
                 <h1 class="blog-post-title">{{ $category_name }}</h1>
                 
                 @include('flash::message')                                                                     
                                                                                       
				 <div class="container">	  
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

					@if(isset($items))  	
						<div class='row products-row'>									
						@foreach($items as $item)                       								
							<div class='col-md-4'>									
								@if (count($item->images) > 0)												
									<a href="{{ url('') }}/products/{{ $item->category->slug }}/{{$item->slug}}">
										<div class="products-row-a">
							               <div class="div-img">
											  <img src="{{ url('') }}/{{ $item->images[0]->location }}" alt="{{ $item->images[0]->name }}">
										   </div>

										   <div class="products-row-txt">
											   <h3>{{ $item->name }}</h3>						   				   					 				   				   					 
											   <h4>${{ number_format($item->price,0) }}</h4>						   					   
										   </div>   
									    </div>
									</a>																						
								@endif
							</div>
					   @endforeach               																										
					   </div>		
				   @else
					   <p>Currently there is no products to display.</p>    
				   @endif
			    </div>
                 
                <!-- Pagination -->
                <div id="pagination"> {{ $items->links() }}</div>                                      
   
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
@endsection
