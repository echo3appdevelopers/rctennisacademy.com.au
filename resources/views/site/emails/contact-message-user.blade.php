<!DOCTYPE html>
<html>
<body>
<h1>Thank you for your message!</h1>
<table class="table">
    @foreach(json_decode($contact_message->data) as $field)
        <tr>
            <th style="width:200px; text-align: left">{{ strip_tags($field->label) }} :</th>
            <td>{{ $field->value }}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan='2'><img src="{{ url('') }}/images/site/email-logo.png"></td>
    </tr>
</table>
</body>
</html>
