<!DOCTYPE html>
<html>
<body>
<h1>New Free Trial Lesson Message</h1>
<table class="table">
    @foreach(json_decode($contact_message->data) as $field)
        <tr>
            <th style="width:200px; text-align: left">{{ strip_tags($field->label) }} :</th>
            <td>{{ $field->value }}</td>
        </tr>
    @endforeach
</table>
</body>
</html>
