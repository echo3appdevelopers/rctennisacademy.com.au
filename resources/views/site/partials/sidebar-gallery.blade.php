<div class="col-sm-3 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">
	<h4>Gallery</h4>
	<ol class="navsidebar list-unstyled">             
	  @foreach ($side_nav as $item)	 
		 <li class='{{ (isset($items) && $item->id == $items[0]->category_id ? "active" : "") }}'><a class="navsidebar" href="{{ url('') }}/gallery/{{ $item->slug }}">{{ $item->name }}</a></li>		
	  @endforeach              
	</ol>
  </div>          
</div>