<footer class='footer'>
 <div class='footerContentWrapper'>
	 <div class='footerContent'>
        <div class="panelNav">
	    <div class="container-fluid">
		<div class="row">
       
           <div class="col-lg-5">
		      <div class="center-block footer-company">
			     RC Tennis Academy			     
			  </div>			  			 
		   </div>  
                	              		   		   
		   
		   <div class="col-lg-2">
			   <div class="center-block footer-address">
			   <h3>Address</h3>
			   {!! $address !!}	
			   </div>
		   </div>
		   
		   <div class="col-lg-2">
			   <div class="center-block footer-phone">
				  <h3>Contact</h3>
			      @if ( $email != "")<a href="mailto:{{ $email }}">{{ $email }}</a><br> @endif 
		   </div>
		  
		</div>				
  
	 </div> 	 	 		
 </div> 
	
 </div>
 
	
  </div>
  
    <div id="footer-txt"> 
		<a href="{{ url('') }}">&copy; {{ date('Y') }} RC Tennis Academy</a> |			   
		<a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a> 

	</div>
</footer>