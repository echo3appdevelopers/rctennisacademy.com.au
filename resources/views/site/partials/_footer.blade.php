<footer class='footer'>
 <div class='footerContainer'>
  <div id="footer-social">
	@if ( $social_facebook != "") <a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook-f'></i></a> @endif
	@if ( $social_twitter != "") <a href="{{ $social_twitter }}" target="_blank"><i class='fab fa-twitter'></i></a> @endif 
	@if ( $social_linkedin != "") <a href="{{ $social_linkedin }}" target="_blank"><i class='fab fa-linkedin-in'></i></a> @endif
	@if ( $social_googleplus != "") <a href="{{ $social_googleplus }}" target="_blank"><i class='fab fa-google-plus-g'></i></a> @endif
	@if ( $social_instagram != "") <a href="{{ $social_instagram }}" target="_blank"><i class='fab fa-instagram'></i></a> @endif
	@if ( $social_pinterest != "") <a href="{{ $social_pinterest }}" target="_blank"><i class='fab fa-pinterest-p'></i></a> @endif
	@if ( $social_youtube != "") <a href="{{ $social_youtube }}" target="_blank"><i class='fab fa-youtube'></i></a> @endif
	<a href="{{ url('') }}"/rss.xml" target="_blank"><i class='fa fa-rss'></i></a> 
	<a href="{{ url('') }}"/contact" ><i class='fa fa-envelope'></i></a>
  </div> 
 
  <div id="footer-txt"> 
	@if ( $company_name != "")<a href="{{ url('') }}">&copy; {{ date('Y') }} {{ $company_name }}</a> | @endif 
    @if ( $phone_number != "") <a href="tel:{{ str_replace(' ', '', $phone_number) }}">{{ $phone_number }}</a> | @endif 
    <a href="{{ url('') }}/contact">Contact Us</a> | 
    <a href="{{ url('') }}">Privacy</a> |
    <a href="{{ url('') }}">Terms</a> |
    <a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a> 
    
  </div>
  
  <div id="footer-support">
     <div>
        This site is lovingly sponsored by
        <a href="https://www.echo3.com.au" title="Echo3"><img src='images/site/echo3.jpg' title="Echo3" alt="Echo3" ></a> 
     </div>
  </div>
  </div>
</footer>