<div class="col-sm-2 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">
	<h4>{{ $category[0]->name }}</h4>
	<ol class="navsidebar list-unstyled">             
	  @foreach ($side_nav as $item)
		 <li class='{{ ($item->slug == $page->slug ? "active" : "") }}'><a class="navsidebar" href="{{ url('') }}/pages/{{ $category[0]->slug }}/{{ $item->slug }}">{{ $item->title }}</a></li>

		 <ol class="navsidebar navsidebar-sub list-unstyled">  
			@foreach ($item->nav_sub as $item_sub)            
			 <li class='{{ ($item_sub->slug == $page->slug ? "active" : "") }}'><a class="navsidebar" href="{{ url('') }}/pages/{{ $category[0]->slug }}/{{ $item_sub->slug }}">{{ $item_sub->title }}</a></li>             
			@endforeach
		 </ol>
	  @endforeach              
	</ol>
  </div>          
</div>