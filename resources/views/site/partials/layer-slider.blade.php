<div class="divLayerSlider" style="">
  <div class="divLayerSlider-caption">Helping the<br>homeless &<br>families in crisis</div>
  <div class="divLayerSlider-support">
     <div>Powered By</div>
     <img src='{{ url('') }}/images/site/echo3 logo.png' title="Echo3" alt="Echo3" >
  </div>
  
  <div id="layer-slider"  style="height:556px">        
    <div class="ls-slide" data-ls="kenburnsscale:1.2;">
      	   
	  
	        
      @php
      $counter=0;
      @endphp
      
      @foreach($images as $image)       
         @php        
            $counter++;
         @endphp
                             			
         @if ($counter == 1) 
			<img src="{{ url('') }}/{{ $image->location }}" class="ls-l layer-slider-img1" alt="{{ $image->title }}" style="border: 20px solid #eee; box-shadow: 0px 4px 30px rgba(0,0,0,.3);position:relative; top:50%; left:50%; text-align:initial; font-weight:400; font-style:normal; text-decoration:none;" data-ls="showinfo:1; offsetxin:left; offsetyin:bottom; durationin:1500; easingin:easeOutQuad; fadein:false; rotatein:10; scalexin:.5; scaleyin:.5; durationout:2000; startatout:allinandloopend + 0; easingout:easeInQuint; loop:true; loopoffsetx:0; loopoffsety:0; loopduration:3000; loopstartat:transitioninstart + 29000; loopeasing:easeInOutQuart; looprotate:20; loopscalex:1.2; loopscaley:1.2; loopcount:1; loopfilter:grayscale(0%) sepia(0%) contrast(150%); rotation:-20; scaleX:.5; scaleY:.5;">                 
          
          @elseif ($counter == 2)
			<img src="{{ url('') }}/{{ $image->location }}" class="ls-l layer-slider-img2" alt="{{ $image->title }}" style="border: 20px solid #eee; box-shadow: 0px 4px 30px rgba(0,0,0,.3);position:relative; top:50%; left:68%; text-align:initial; font-weight:400; font-style:normal; text-decoration:none;" data-ls="showinfo:1; offsetxin:right; offsetyin:top; durationin:1500; delayin:100; easingin:easeOutQuad; fadein:false; rotatein:30; scalexin:.5; scaleyin:.5; durationout:2000; startatout:allinandloopend + 0; easingout:easeInQuint; loop:true; loopoffsetx:0; loopoffsety:0; loopduration:3000; loopstartat:transitioninstart + 26000; loopeasing:easeInOutQuart; looprotate:-10; loopscalex:1.2; loopscaley:1.2; loopcount:1; loopfilter:grayscale(0%) sepia(0%) contrast(150%); rotation:10; scaleX:.5; scaleY:.5;">         

          @elseif ($counter == 3)						
			<img src="{{ url('') }}/{{ $image->location }}" class="ls-l layer-slider-img3" alt="{{ $image->title }}" style="border: 20px solid #eee; box-shadow: 0px 4px 30px rgba(0,0,0,.3);position:relative; top:50%; left:86%; text-align:initial; font-weight:400; font-style:normal; text-decoration:none;" data-ls="showinfo:1; offsetxin:right; offsetyin:bottom; durationin:1500; delayin:200; easingin:easeOutQuad; fadein:false; rotatein:30; scalexin:.5; scaleyin:.5; durationout:2000; startatout:allinandloopend + 0; easingout:easeInQuint; loop:true; loopoffsetx:0; loopoffsety:0; loopduration:3000; loopstartat:transitioninstart + 23000; loopeasing:easeInOutQuart; looprotate:5; loopscalex:1.2; loopscaley:1.2; loopcount:1; loopfilter:grayscale(0%) sepia(0%) contrast(150%); rotation:-5; scaleX:.5; scaleY:.5;">	
		   @endif		          		   		   					
	  @endforeach	
	  
	      	  		
		</div>
	</div>
</div>
	
@section('inline-scripts-slider')
<script type="text/javascript">   
    $(document).ready(function(){        
        $("#layer-slider").on('slideTimelineDidComplete', function( event, slider ) {
				slider.api( 'replay' );

			});

		$('#layer-slider').layerSlider({
			sliderVersion: '6.6.4',
			type: 'fullwidth',
			fitScreenWidth: 'true',
			fullSizeMode: 'normal',
			skin: 'v6',
			//globalBGImage: '{{ url('') }}/images/site/slider-background.jpg',
			globalBGSize: 'cover',
			showCircleTimer: false,
			allowRestartOnResize: true,
			skinsPath: '../../layerslider/skins/',
			//height: 1000
		});
    });
 
</script>
@endsection