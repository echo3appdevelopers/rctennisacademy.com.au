 <div class="home-pages">
   <div class="container">
	  <div class="row">   		
		      <div class="col-lg-1"></div>
		      
			  <div class="col-lg-2">
			       <div class="home-pages-box">
					   <div class="home-pages-img">
						  <img src="{{ url('') }}/images/site/1-ANZ-Hot-Shots.jpg" alt="" />
					   </div>

					   <h2>ANZ HOT SHOTS</h2>
					   <p>Fun way to play tennis</p>
					   <p>Modified equipment</p>
					   <p>Develop motor skills and coordination</p>
					   <p>Stay active</p>
					   <p>Make friends</p>				   

					   <p><a class="btn-submit" href="{{ url('') }}/" role="button">Find out more</a></p>
				   </div>

			  </div><!-- /.col-lg-2 -->	
			  
			  <div class="col-lg-2">
			       <div class="home-pages-box">
					   <div class="home-pages-img">
						  <img src="{{ url('') }}/images/site/2-Cardio-Tennis.jpg" alt="" />
					   </div>

					   <h2>CARDIO TENNIS</h2>
					   <p>Mix of cardio workouts and modified tennis drills</p>
					   <p>Suitable for all skills and fitness levels</p>
					   <p>Raise a happy sweat</p>

					   <p><a class="btn-submit" href="{{ url('') }}/" role="button">Find out more</a></p>
				   </div>

			  </div><!-- /.col-lg-2 -->
			  
			  <div class="col-lg-2">
			       <div class="home-pages-box">
					   <div class="home-pages-img">
						  <img src="{{ url('') }}/images/site/3-Private-Lessons.jpg" alt="" />
					   </div>

					   <h2>PRIVATE LESSONS</h2>
					   <p>One on one</p>
					   <p>Enhance your skills</p>
					   <p>Focus on your individual needs and wants</p>

					   <p><a class="btn-submit" href="{{ url('') }}/" role="button">Find out more</a></p>
				   </div>

			  </div><!-- /.col-lg-2 -->
			  
			  <div class="col-lg-2">
			       <div class="home-pages-box">
					   <div class="home-pages-img">
						  <img src="{{ url('') }}/images/site/4-Holiday-Clinics.jpg" alt="" />
					   </div>

					   <h2>HOLIDAY CLINICS</h2>
					   <p>Fun and relaxed</p>
					   <p>Stay active</p>
					   <p>Technical</p>
					   <p>Match play</p>
					   <p>Friendship</p>

					   <p><a class="btn-submit" href="{{ url('') }}/" role="button">Find out more</a></p>
				   </div>

			  </div><!-- /.col-lg-2 -->
			  
			  <div class="col-lg-2">
			       <div class="home-pages-box">
					   <div class="home-pages-img">
						  <img src="{{ url('') }}/images/site/5-Restringing.jpg" alt="" />
					   </div>

					   <h2>RESTRINGING</h2>
					   <p>Maintain and maximise your performance</p>
					   <p>Best strings for your racket and game style</p>
					   <p>Quality string</p>
					   <p>Affordable prices</p>

					   <p><a class="btn-submit" href="{{ url('') }}/" role="button">Find out more</a></p>
				   </div>

			  </div><!-- /.col-lg-2 -->
			  
			  <div class="col-lg-1"></div>		
		</div>
   </div>
</div>