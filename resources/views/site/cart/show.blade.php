@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row"> 
        @include('site/partials/sidebar-products')
                          
        <div class="col-sm-8 blog-main">

          <div class="blog-post">           
            <h1 class="blog-post-title">Shopping Cart</h1>
            
               @if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
				@endif

				@if(Cart::isEmpty())
					<div class="alert alert-info">
						Your cart is empty
					</div>
				@else
					<table class="table">
						<thead>
							<tr>
								<th>name</th>
								<th class="td-right">price</th>
								<th class="td-center">qty</th>
								<th class="td-right">total</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach(Cart::getItems() as $item)
								<tr>
									<!--<td width="55"><img src="/images/product.jpg" class="product-image"/></td>-->
									<td>{{ $item->product->getName() }}</td>
									<td class="td-right">{{ format_price($item->price) }}</td>
									<td class="td-center">{{ $item->quantity }}</td>
									<td class="td-right">{{ format_price($item->total) }}</td>
									<td>
										<form action="{{ route('cart.remove', $item) }}"
											  style="display: inline-block" method="post">
											{{ csrf_field() }}
											<button class="btn btn-link btn-sm btn-cart-remove"><i class="fas fa-times"></i> remove</button>
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
						<tfoot>
						<tr>
							<th colspan="3">includes free shipping</th>
							<th>
								{{ format_price(Cart::total()) }}
							</th>
							<th></th>
						</tr>
						</tfoot>

					</table>

					<p class="text-right">
						<a class="btn btn-link btn-sm btn-cart-shopping" href="{{ url('') }}/products" class="btn btn-lg btn-link">continue shopping</a>
						<a class="btn-checkout" href="{{ route('checkout.show') }}" class="btn btn-lg btn-primary">Proceed To Checkout</a>
					</p>

				@endif
            
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
@endsection
                                                                      