@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-documents')        
        
        <div class="col-sm-8 blog-main">
          @if(isset($category)) 
          <h1>{{ $category->name }}</h1>
          
          @if($category->description != "")
             {!! $category->description !!}
          @endif
          @endif
                 
		  @if(isset($items))        
			 @foreach ($items as $item)	
				<div class="blog-post">   
	           
		           <div class='document-wrapper'>
					   <div class='document-title'>
						  <h3>{{$item->title}}</h3>
					   </div>

					   <div class='document-link'>
						  <a href='{{ url('') }}{{$item->fileName}}' target='_blank'><i class="fa fa-file"></i> View Document</a>
					   </div>				   
			   
			           <div class='document-details'>
						   <b>Date:</b> {{ date('j M Y' , strtotime($item->dateDocument)) }}
					   </div>	
					   
					   <div class='document-details'>
						  {!! $item->description !!} 
					   </div>			                                     					 
				   </div>
				                                     					
				</div><!-- /.blog-post -->         
		 	 @endforeach 
		  @endif			     
                               
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
@endsection
