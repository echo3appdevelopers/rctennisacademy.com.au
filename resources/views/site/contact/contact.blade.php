Stay Connected:Stay Connected:@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="panel">
   <div class="blog-masthead ">         
    <div class="container">

      <div class="row">      
        
        <div class="col-sm-9 blog-main blog-main-full">

          <div class="blog-post">   
               <h1 class="blog-post-title">Send us a message</h1> 
               <p style="text-align: center;"><strong>"I always work with a goal - and the goal is to improve as a player and a person.  That, finally, is the most important thing of all".<br>- Rafael Nadal.</strong></p>
               <p>Thank you for visiting Ringwood Central Tennis Academy.  Please complete the form below to send us a message.  We hope to see you on the court soon!</p>                
            </div>
            <div class="col-lg-9 p-0">
                <form id="contact-form" method="post" action="{{ url('contact/save-message') }}" class="contact-page">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div id="contact-form-fields-page"></div>

                    <div class="form-row">
                        <div class="col-12 col-sm-10 g-recaptcha-container">
                            <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                            @if ($errors->has('g-recaptcha-response'))
                                <div class="fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
                            @endif
                        </div>
                    </div>
                    <button type="submit" class="btn-submit">Send</button>
                </form>
                
                <div class="divContact">
					<p><strong>Maria Vallos - Director and Head Coach</strong><br>
					   <a href="mailto:coaching@rctennisacademy.com.au">coaching@rctennisacademy.com.au</a><br>
					   <a href="tel:0407851979">0407 851 979</a><br><br>
					   Stay Connected:<br>
					   <a href="https://www.facebook.com/RCtennisacademy/" target="_blank"><i class="fab fa-facebook-f"></i></a><br>					   
					   <a href="https://www.rctennisclub.com/" target="_blank">Ringwood Central Tennis Club</a><br>
					</p>
				</div>
            </div>
        </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
	</div><!-- /.blog-masthead -->		
</div><!-- /.panel -->

<iframe allowfullscreen="" aria-hidden="false" frameborder="0" height="400" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.578605539169!2d145.23022061531896!3d-37.82333822975066!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad63bdbf8b3e60b%3A0xf04567605318800!2sJubilee%20Park!5e0!3m2!1sen!2sau!4v1599704292661!5m2!1sen!2sau" style="border:0;" tabindex="0" width="100%"></iframe>
@endsection
@section('scripts')
    <script src="{{ asset('/components/formBuilder/dist/form-render.min.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
@section('inline-scripts-contact-page')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#contact-form-fields-page').formRender({
                dataType: 'json',
                formData: {!! $contact_form !!},
                notify: {
                    success: function(message) {

                        FormValidation.formValidation(
                            document.getElementById('contact-form'),
                            {
                                plugins: {
                                    declarative: new FormValidation.plugins.Declarative({
                                        html5Input: true,
                                    }),
                                    submitButton: new FormValidation.plugins.SubmitButton(),
                                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                                    bootstrap: new FormValidation.plugins.Bootstrap(),
                                    icon: new FormValidation.plugins.Icon({
                                        valid: 'fa fa-check',
                                        invalid: 'fa fa-times',
                                        validating: 'fa fa-refresh',
                                    })
                                },
                            }
                        );

                    }
                }
            });

        });
    </script>
@endsection
