@extends('site/layouts/app')

@section('content')

@include('site/partials/layer-slider')


<div class="panel">

	@include('site/partials/index-pages')

	<hr>

	<!-- Intro Text -->
	<div class="row featurette">
	  <div class="col home-intro">
		<?php echo $home_intro_text; ?>
	  </div>
	</div>
	
	@include('site/partials/index-video')
	
	<hr>
	
	<!-- Donate Text -->
	<div class="row featurette">
      <div class="col-lg-6">	
		@include('site/partials/index-facebook')
	  </div>
	  
	  <div class="col-lg-6 donate-intro">	
		<?php echo $home_donate_text; ?>
		<p><a class="btn-submit btn-donate" href="{{ url('') }}" role="button">What should I donate</a></p>
		<img src="{{ url('') }}/images/site/map.jpg" title="Map" alt="Map">
	  </div>
	</div>		

</div>

@endsection
