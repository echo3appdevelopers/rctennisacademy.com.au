@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Home Page Sliders</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/settings/home-sliders') }}"><i class="fa fa-picture-o"></i> Settings</a></li>
                <li><a href="{{ url('dreamcms/settings/home-sliders') }}"> Home Page Sliders</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Image</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/settings/update-home-slider') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $image->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title" placeholder="Title" value="{{ $image->title }}">
                                        @if ($errors->has('title'))
                                            <small class="help-block">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div>    
                                
                                <div class="form-group {{ ($errors->has('description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Description</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="description" placeholder="Description">{{ $image->description }}</textarea>
                                        @if ($errors->has('description'))
                                            <small class="help-block">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>
                                </div> 
                                
                                <div class="form-group{{ ($errors->has('url')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">URL Link</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="url" placeholder="URL Link" value="{{ $image->url }}">
                                        @if ($errors->has('url'))
                                            <small class="help-block">{{ $errors->first('url') }}</small>
                                        @endif
                                    </div>
                                </div>                                                              
                                
                                <div class="form-group {{ ($errors->has('location')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Image *</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="location" name="location" value="{{ $image->location }}">
                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload Image</button>
                                        @php
                                            $class = ' invisible';
                                            if($image->location){
                                                $class = '';
                                            }
                                        @endphp
                                        <button id="remove-image" type="button" class="btn btn-danger btn-sm{{ $class }}">Remove Image</button>
                                        <br/><br/>
                                        <span id="added_image">
                                        @if($image->location)
                                                <image src="{{ $image->location }}" style='max-width: 100%;' />
                                            @endif
                                        </span>
                                        @if ($errors->has('location'))
                                            <small class="help-block">{{ $errors->first('location') }}</small>
                                        @endif
                                    </div>
                                </div>
                                                                
                                
                            </div>
                            
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">Status *</label>
                                    <div class="col-sm-10">
                                        <label>                                       
                                         <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini" name="live" {{ $image->status == 'active' ? ' checked' : null }}>                                    
                                        </label>
                                    </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{url()->previous()}}" class="btn btn-info pull-right">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">Save & Close</button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/ckfinder/ckfinder.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $('input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });
			
			$( "#image-popup" ).click(function() {
                openPopup();
            });

            $( "#remove-image" ).click(function() {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#location').val('');
            });
        });
		
		function openPopup() {
            CKFinder.popup( {
                chooseFiles: true,
                onInit: function( finder ) {
                    finder.on( 'files:choose', function( evt ) {
                        var file = evt.data.files.first();
                        $('#added_image').html('<image src="'+base_url+file.getUrl()+'" style="max-width: 100%;">');
                        $('#remove-image').removeClass('invisible');
                        $('#location').val(file.getUrl());

                    } );
                    finder.on( 'file:choose:resizedImage', function( evt ) {
                        $('#added_image').html('<image src="'+base_url+evt.data.resizedUrl+'" style="max-width: 100%;">');
                        $('#remove-image').removeClass('invisible');
                        $('#location').val(evt.data.resizedUrl);
                    } );
                }
            } );
        }
    </script>
@endsection