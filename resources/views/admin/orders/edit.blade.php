@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Orders</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/orders') }}"><i class="fas fa-shopping-cart"></i> Orders</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Order</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/orders/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $order->id }}">
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-sm-2 orders-header">Order Number</div>
                                    <div class="col-sm-10">{{ $order->number }}</div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 orders-header">Order Date</label>
                                    <div class="col-sm-10">{{ date('d/m/Y H:i:s' , strtotime($order->created_at)) }}</div>
                                </div>
                                
                                @php
                                   $billToState = "";
                                   foreach ($states as $state)  {
                                      if ($state->id == $order->billpayers->addresses->province_id)  {
                                         $billToState = $state->state;
                                      }                                      
                                   }
                                @endphp
                                
                                <div class="form-group">
									<div class="col-sm-6">
									    <div class="orders-card"> 
											<div class="orders-card-header">Bill To</div>
											<div class="orders-card-block">
												{!! ($order->billpayers->company_name != "" ? '<strong>' . $order->billpayers->company_name . '</strong><br>' : '') !!}
												<strong>{{ $order->billpayers->firstname }} {{ $order->billpayers->lastname }}</strong><br>												
												{{ $order->billpayers->addresses->address }}<br>
												@if ($order->billpayers->addresses->address2 != "") {{ $order->billpayers->addresses->address2 }}<br> @endif
												{{ $order->billpayers->addresses->city }} {{ $billToState }} {{ $order->billpayers->addresses->postalcode }}<br>
												Australia<br><br>
												<strong>{{ $order->billpayers->email }}</strong><br>
											</div>
										</div>
									</div>
									
									@php
									   $shipToState = "";
									   foreach ($states as $state)  {
										  if ($state->id == $order->shiptoaddresses->province_id)  {
											 $shipToState = $state->state;
										  }                                      
									   }
									@endphp

									<div class="col-sm-6">
									    <div class="orders-card"> 
										    <div class="orders-card-header">Ship To</div>
											<div class="orders-card-block">                                        
												<strong>{{ $order->shiptoaddresses->name }}</strong><br>
												{{ $order->shiptoaddresses->address }}<br>
												@if ($order->shiptoaddresses->address2 != "") {{ $order->shiptoaddresses->address2 }}<br> @endif
												{{ $order->shiptoaddresses->city }} {{ $shipToState }} {{ $order->shiptoaddresses->postalcode }}<br>
												Australia
											</div>
										</div>
									</div>                                                               
								</div>                                
                                                                                                                                                                                                                              
                                <div class="orders-card">   
                                   <div class="orders-card-header">Ordered Items</div> 
                                   
                                   <div class="orders-card-block">                                      
                                    <table class="orders-table">
                                       <tr>
                                       	  <th>#</th>
                                       	  <th>Name</th>                                       	  
                                       	  <th>Qty</th>
                                       	  <th class="orders-table-price">Price</th>
                                       	  <th class="orders-table-price">Subtotal</th>
                                       </tr>
                                       
                                       @php 
                                          $itemCounter = 0;
                                          $orderTotal = 0;
                                       @endphp
                                       
                                       @foreach($order->items as $item)
                                           @php 
                                              $itemCounter++;
                                              $orderTotal += $item->quantity * $item->price;
                                           @endphp
                                           
										   <tr>
											  <td>{{ $itemCounter }}</th>
											  <td>{{ $item->name }}</td>                                  	  
											  <td>{{ $item->quantity }}</td>
											  <td class="orders-table-price">${{ number_format($item->price,2) }}</td>
											  <td class="orders-table-price">${{ number_format($item->quantity * $item->price,2) }}</td>
										   </tr>
                                       @endforeach
                                       
                                       <tr>
										  <td colspan='4' class="orders-table-total orders-table-price">Order Total</th>										  
										  <td class="orders-table-total orders-table-price">${{ number_format($orderTotal,2) }}</td>
									   </tr>		
                                    </table> 
								</div>
                                                                  
                                </div>
                                
                            @php
								if(count($errors)>0){                               
									$paid_status = old('paid_status');                               
								}else{
									$paid_status = $order->paid_status;
								}                            
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 orders-header">Payment Status *</label>
                                <div class="col-sm-10">
                                    <select name="paid_status" class="form-control select2" style="width: 100%;">                                           
									   <option value="pending"{{ ($paid_status == 'pending') ? ' selected="selected"' : '' }}>Pending</option>
									   <option value="completed"{{ ($paid_status == 'completed') ? ' selected="selected"' : '' }}>Completed</option>                                            
									   <option value="cancelled"{{ ($paid_status == 'cancelled') ? ' selected="selected"' : '' }}>Cancelled</option>									  
									</select>
                                </div>
                            </div>
                               
                            @php if ($order->paid_transaction_number != "") { @endphp
                            <div class="form-group">
                                <label class="col-sm-2 orders-header">eWAY Transaction</label>
                                <div class="col-sm-10">
                                    {{ $order->paid_transaction_number }} 
                                    <span class='{{ ($order->paid_transaction_result == "Successful" ? 'sp-success' : 'sp-error') }}'> {!! ($order->paid_transaction_result == "Successful" ? '<i class="fas fa-check"></i>' : '<i class="fas fa-times"></i>') !!} {{ $order->paid_transaction_result }}</span>
                                </div>
                            </div>       
                            @php } @endphp
                                                                                                                                                    
                            @php
								if(count($errors)>0){                               
									$status = old('state');                               
								}else{
									$status = $order->status;
								}                            
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 orders-header">Status *</label>
                                <div class="col-sm-10">
                                    <select name="status" class="form-control select2" style="width: 100%;">                                           
									   <option value="pending"{{ ($status == 'pending') ? ' selected="selected"' : '' }}>Pending</option>
									   <option value="completed"{{ ($status == 'completed') ? ' selected="selected"' : '' }}>Completed</option>                                            
									   <option value="cancelled"{{ ($status == 'cancelled') ? ' selected="selected"' : '' }}>Cancelled</option>									  
									</select>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/orders') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {           
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $(".select2").select2();
            
        });
    </script>
@endsection