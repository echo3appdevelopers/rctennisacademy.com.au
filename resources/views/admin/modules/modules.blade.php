@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/ui-darkness/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Modules</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-cubes"></i> Modules</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-body">
                    @if(count($modules))
                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Live</th>
                            </tr>
                            <tbody>
                            @foreach($modules as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->display_name }}</td>
                                    <td>
                                        <input id="module_{{ $item->id }}" data-id="{{ $item->id }}" class="module_status" type="checkbox" data-toggle="toggle" data-size="mini"{{ $item->status == 'active' ? ' checked' : null }}>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        No record
                    @endif
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            $('.module_status').change(function() {
                $.ajax({
                    type: "POST",
                    url: "modules/"+$(this).data('id')+"/change-status",
                    data:  {
                        'status':$(this).prop('checked')
                    },
                    success: function (response) {
                        if(response.status=="success"){
                            toastr.options = {"closeButton": true}
                            toastr.success('Status has been changed');
                        }
                    }
                });
            });
        });
    </script>
@endsection