@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Member Types</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/members/types') }}"><i class="fa fa-users"></i> Member Types</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="col-sm-12 col-md-10 col-lg-8">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Type</h3>
                    </div>

                    <form method="post" class="form-horizontal" action="{{ url('dreamcms/members/update-type') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $type->id }}">
                        <div class="box-body">
                            <div class="form-group {{ ($errors->has('name')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Name *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="name" placeholder="Type Name"
                                           value="{{ old('name',$type->name) }}">
                                    @if ($errors->has('bank_name'))
                                        <small class="help-block">{{ $errors->first('name') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ ($errors->has('price')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Price *</label>

                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fas fa-dollar-sign"></i></span>
                                        <input type="text" class="form-control money" name="price" placeholder="Price"
                                               value="{{ old('price', $type->price) }}">
                                    </div>
                                    @if ($errors->has('price'))
                                        <small class="help-block">{{ $errors->first('price') }}</small>
                                    @endif
                                </div>
                            </div>
                            @php
                                if(count($errors)>0){
                                   if(old('live')=='on'){
                                    $status = 'active';
                                   }else{
                                    $status = '';
                                   }
                                }else{
                                    $status = $type->status;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status *</label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="live" {{ $status == 'active' ? ' checked' : '' }}>
                                    </label>
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <a href="{{ url('dreamcms/members/types') }}" class="btn btn-info pull-right"
                               data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                               data-btn-cancel-label="No">Cancel</a>
                            <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">Save
                                & Close
                            </button>
                            <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/jquery-mask-plugin/dist/jquery.mask.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.money').mask('#,##0.00', {reverse: true});

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection