@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/ui-darkness/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Gallery</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fas fa-images"></i> Gallery</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <form method="post" action="{{ url('dreamcms/gallery') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">

                            <div class="form-group col-xs-2">
                                <select name="category" class=" select2" style="width: 100%;">
                                    <option value="all" {{ $session['category'] == "" || $session['category']=="all" ? ' selected="selected"' : '' }}>
                                        All Categories
                                    </option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}"{{ $session['category'] == $category->id ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-xs-2">
                                <input type="text" class="form-control" name="search" placeholder="Search" value="{{ $session['search'] }}">
                            </div>

                            <div class="form-group col-xs-3 filter-button">
                                <button type="submit" class="btn btn-info">Filter</button>
                                @if($is_filtered)
                                    <a href="{{ url('dreamcms/gallery/forget') }}" type="submit" class="btn btn-danger">Remove</a>
                                @endif
                            </div>
                        </div>

                    </form>
                    Please select category to enable image sort

                    @can('add-image')
                    <div class="pull-right box-tools">
                        <a href="{{ url('dreamcms/gallery/add') }}" type="button" class="btn btn-info btn-sm"
                           data-widget="add">Add New Image
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                    @endcan
                </div>
                <div class="box-body" {!! $session['category'] == "" || $session['category']=="all" ? '' : ' id="sortable"' !!}>
                    @if(count($images))

                        @foreach($images as $image)
                        <div class="image-box" id='item_{{ $image->id }}'>
                            <div class="image-box-toolbox">

                                @can('edit-image')
                                <a href="{{ url('dreamcms/gallery/'.$image->id.'/edit') }}" class="tool" data-toggle="tooltip" title="Edit">
                                    <i class="fa fa-edit"></i>
                                </a>
                                @endcan
                                @can('delete-image')
                                <a href="{{ url('dreamcms/gallery/'.$image->id.'/delete') }}"
                               class="tool" data-toggle=confirmation data-title="Are you sure?"
                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                               data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
                                @endcan
                                @can('edit-image')
                                <div class="pull-right" style="margin-right: 10px">
                                  <input id="img_{{ $image->id }}" data-id="{{ $image->id }}" class="image_status" type="checkbox" data-toggle="toggle" data-size="mini"{{ $image->status == 'active' ? ' checked' : null }}>
                               </div>
                               @endcan

                            </div>
                            <div class="image-box-img">
                                <image src="{{ url('/').$image->location }}" class="image-item">
                            </div>
                            <div class="image-box-footer">{{ $image->name }}</div>
                        </div>
                        @endforeach

                    @else
                        No records
                    @endif
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <form id="pagination_count_form" method="post" class="form-inline" action="{{ url('dreamcms/pagination') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="pagination_count" name="pagination_count">
                                    <option value="25"{{ Session::get('pagination-count')==25 ? ' selected="selected"' : '' }}>25</option>
                                    <option value="50"{{ Session::get('pagination-count')==50 ? ' selected="selected"' : '' }}>50</option>
                                    <option value="100"{{ Session::get('pagination-count')==100 ? ' selected="selected"' : '' }}>100</option>
                                </select>
                                <span class="total-row"> Total {{ $images->total() }} images</span>
                            </form>
                        </div>
                        <div class="col-xs-6" style="text-align: right;">
                            {{ $images->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $( "#sortable" ).sortable({
                update: function(event, ui) {
                    $.ajax({
                        type:'POST',
                        url:'gallery/image-sort',
                        data: $(this).sortable('serialize'),
                        success:function(response){
                            if(response.status=="success"){
                                toastr.options = {"closeButton": true}
                                toastr.success('Saved');
                            }
                        }
                    });
                }
            });

            $("#pagination_count").select2({
                minimumResultsForSearch: -1
            });

            $("#pagination_count").change(function() {
                $("#pagination_count_form").submit();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('.image_status').change(function() {
                $.ajax({
                    type: "POST",
                    url: "gallery/"+$(this).data('id')+"/change-status",
                    data:  {
                        'status':$(this).prop('checked')
                    },
                    success: function (response) {
                        if(response.status=="success"){
                            toastr.options = {"closeButton": true}
                            toastr.success('Status has been changed');
                        }
                    }
                });
            });

        });
    </script>
@endsection