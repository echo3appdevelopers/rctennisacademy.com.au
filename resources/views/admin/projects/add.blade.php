@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/ui-darkness/jquery-ui.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Projects</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/projects') }}"><i class="fa fa-clipboard"></i> Projects</a></li>
                <li class="active">Add New</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Projects</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/projects/store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title" id="title"
                                               placeholder="Title" value="{{ old('title') }}">
                                        @if ($errors->has('title'))
                                            <small class="help-block">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('slug')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">SEO Name *</label>
                                    <div class="col-sm-10">

                                        <div class="input-group">
                                            <input type="text" id="slug" name="slug" class="form-control"
                                                   value="{{ old('slug') }}" readonly>
                                            <span class="input-group-btn">
                                          <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                  data-target="#change-slug">Change SEO Name
                                          </button>
                                        </span>
                                        </div>

                                        @if ($errors->has('slug'))
                                            <small class="help-block">{{ $errors->first('slug') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('category_id')) ? ' has-error' : '' }}"
                                     id="category_selector">
                                    <label class="col-sm-2 control-label">Category *</label>

                                    <div class="col-sm-10{{ ($errors->has('category_id')) ? ' has-error' : '' }}">
                                        @if(count($categories)>0)
                                        <select name="category_id" class="form-control select2"
                                                data-placeholder="All" style="width: 100%;">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"{{ (old('category_id') == $category->id) ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                        @else
                                            <div class="callout callout-danger">
                                                <h4>No category found!</h4>
                                                <a href="{{ url('dreamcms/projects/add-category') }}">Please click here to
                                                    add category</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('meta_title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Title *</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" id="meta_title" name="meta_title" class="form-control"
                                                   placeholder="Meta Title"
                                                   value="{{ old('meta_title') }}" maxlength="56">
                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                      id="copy-title" name="copy-title">Copy From Title
                                              </button>
                                            </span>
                                        </div>
                                        <div id="c_count_meta_title">0 characters | 56 characters left</div>
                                        @if ($errors->has('meta_title'))
                                            <small class="help-block">{{ $errors->first('meta_title') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('meta_keywords')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Keywords *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="meta_keywords"
                                                  placeholder="Meta Keywords">{{ old('meta_keywords') }}</textarea>
                                        @if ($errors->has('meta_keyword'))
                                            <small class="help-block">{{ $errors->first('meta_keywords') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ ($errors->has('meta_description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Description *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" id="meta_description"
                                                  name="meta_description" maxlength="250"
                                                  placeholder="Meta Description">{{ old('meta_description') }}</textarea>
                                        <div id="c_count_meta_description">0 characters | 250 characters left | 0
                                            words
                                        </div>
                                        @if ($errors->has('meta_description'))
                                            <small class="help-block">{{ $errors->first('meta_description') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ ($errors->has('short_description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Short Description *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="short_description"
                                                  placeholder="Short Description">{{ old('short_description') }}</textarea>
                                        @if ($errors->has('short_description'))
                                            <small class="help-block">{{ $errors->first('short_description') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Description *</label>

                                    <div class="col-sm-10">
                                        <textarea id="description" name="description" rows="10" cols="80"
                                                  style="height: 500px;">{{ old('description') }}</textarea>
                                        @if ($errors->has('description'))
                                            <small class="help-block">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('thumbnail')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Images</label>
                                    <div class="col-sm-10">                                        
                                        <input type="hidden" id="imageCount" name="imageCount" value="{{ (old('imageCount') != "" ? old('imageCount') : 0) }}">
                                        
                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload
                                            Images
                                        </button>
                                        @php
                                        $class = ' invisible';
                                        if(old('imageCount') && old('imageCount') > 0){
                                            $class = '';
                                        }
                                        $i=0;
                                        @endphp
                                        <br/><br/>
                                        <span id="added_image">                                      
											@foreach($images as $image)
												<div id="imgBox{{ $i }}" class="image-box sortable">
                                                <input id="imgPosition{{ $i }}" name="imgPosition{{ $i }}" class="imgPosition" type="hidden" value="{{ $image->position }}">
											    <input id="imgDelete{{ $i }}" name="imgDelete{{ $i }}" type="hidden" value="{{ $image->delete }}">
											    <input id="imgType{{ $i }}" name="imgType{{ $i }}" type="hidden" value="new">
												<div class="image-box-toolbox">
												<a class="tool" title="Delete" href="#" onclick="image_delete({{ $i }}); return false;">
												<i class="fa fa-trash-alt"></i>
												</a>

												<div class="pull-right" style="margin-right: 10px">
												<div style="width: 33px; height: 22px;">
												@php
												  $status = 'active';											
												  if(old('imgStatus' . $i)=='on'){
												   $status = 'active';
												  }else{
												   $status = '';
												  }											
												@endphp
												<input id="imgStatus{{ $i }}" name="imgStatus{{ $i }}" data-id="{{ $i }}" type="checkbox" class="image_status" data-size="mini" {{ ($image->status == 'active' || $image->status == 'on') ? ' checked' : null }}>
												</div>														
												</div>

												</div>

												<div class="image-box-img">
												<image class="image-item" src="{{ old('imgLocation' . $i)}}" />
												<input id="imgLocation{{ $i }}" name="imgLocation{{ $i }}" type="hidden" value="{{ old('imgLocation' . $i)}}">
												</div>

												</div>
                                                @php
                                                $i++;
                                                @endphp
                                            @endforeach
                                        </span>                                        
                                    </div>
                                </div>
                               
                                @php
                                    $status = 'active';
                                    if(count($errors)>0){
                                       if(old('live')=='on'){
                                        $status = 'active';
                                       }else{
                                        $status = '';
                                       }
                                    }
                                @endphp
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status *</label>
                                    <div class="col-sm-10">
                                        <label>
                                            <input class="page_status" type="checkbox" data-toggle="toggle"
                                                   data-size="mini" name="live" {{ $status == 'active' ? ' checked' : null }}>
                                        </label>
                                    </div>
                                </div>								
                               
                                <div class="box-footer">
                                    <a href="{{ url('dreamcms/news') }}" class="btn btn-info pull-right">Cancel</a>
                                    <button type="submit" class="btn btn-info pull-right" name="action"
                                            value="save_close">Save & Close
                                    </button>
                                    <button type="submit" class="btn btn-info pull-right" name="action" value="save">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change SEO Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" id="slug-modal" name="slug-modal" value="{{ old('slug') }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/ckfinder/ckfinder.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            CKEDITOR.replace('description');
            CKEDITOR.replace('short_description');
            $(".select2").select2();			
			
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            var maxLengthTitle = 56;
            $('#meta_title').keyup(function () {
                var textlen = maxLengthTitle - $(this).val().length;
                $('#c_count_meta_title').text($(this).val().length + " characters | " + textlen + " characters left");
            });

            var maxLengthDescription = 250;
            $('#meta_description').keyup(function () {
                var textlen = maxLengthDescription - $(this).val().length;
                var words = $(this).val().trim().split(" ").length;
                $('#c_count_meta_description').text($(this).val().length + " characters | " + textlen + " characters left | " + words + " words");
            });

            $('#title').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
                $('#slug').val(slug);
            });

            $('#slug-modal').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
            });

            $("#save-seo").click(function () {
                var slug = $('#slug-modal').val();
                $('#slug').val(slug);
                $('#change-slug').modal('toggle');
            });

            $('#copy-title').click(function () {
                $('#meta_title').val($('#title').val().substr(0, maxLengthTitle));
                $('#meta_title').trigger("keyup", {which: 50});
            });

            $("#image-popup").click(function () {
                openPopup();
            });

            $("#remove-image").click(function () {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#thumbnail').val('');
				$('#imageCount').val(0);
            });
        });
        
		$('#added_image').sortable({  
			items: '.sortable',
			
			stop: function () {											
				counter = 0;
				$('input.imgPosition').each(function() {           
                   $(this).val(counter);
				   counter++;
                });				
			}
		});
		
		for (i = 0; i < $('#imageCount').val(); i++) {
		    $('#imgStatus' + i).bootstrapToggle();	
		}
		
		function openPopup() {	    			
            imageCounter = $('#imageCount').val();
			
			CKFinder.popup( {
				chooseFiles: true,
				width: 800,
				height: 600,		

				onInit: function( finder ) {			

					finder.on( 'files:choose', function( evt ) {
						var files = evt.data.files;										

						files.forEach( function( file, i ) {					
							folder = file.get( 'folder' );

							strHtml  = '<div id="imgBox' + imageCounter + '" class="image-box sortable">';							
							
							strHtml += '<input id="imgPosition' + imageCounter + '" name="imgPosition' + imageCounter + '" type="hidden" class="imgPosition" value="' + imageCounter + '">';
                            strHtml += '<input id="imgDelete' + imageCounter + '" name="imgDelete' + imageCounter + '" type="hidden" value="">';
							strHtml += '<input id="imgType' + imageCounter + '" name="imgType' + imageCounter + '" type="hidden" value="new">';
							strHtml += '<div class="image-box-toolbox">';							
							
							strHtml += '<a class="tool" title="Delete" href="#" onclick="image_delete(' + imageCounter + '); return false;">';
							strHtml += '<i class="fa fa-trash-alt"></i>';
							strHtml += '</a>';

							strHtml += '<div class="pull-right" style="margin-right: 10px">';
							strHtml += '<div style="width: 33px; height: 22px;">';
							strHtml += '<input id="imgStatus' + imageCounter + '" name="imgStatus' + imageCounter + '" data-id="' + imageCounter + '" type="checkbox" class="image_status" data-size="mini" checked>';
							strHtml += '</div>';														
							strHtml += '</div>';

							strHtml += '</div>';

							strHtml += '<div class="image-box-img">';
							strHtml += '<image class="image-item" src="' + folder.getUrl() + file.get( 'name' ) + '">';	
							strHtml += '<input id="imgLocation' + imageCounter + '" name="imgLocation' + imageCounter + '" type="hidden" value="' + folder.getUrl() + file.get( 'name' ) + '">';
							
							strHtml += '</div>';

							strHtml += '</div>';

							$('#added_image').append(strHtml);
                            $('#imgStatus' + imageCounter).bootstrapToggle();
							
							imageCounter++;
							
							$('#imageCount').val(imageCounter);
							$('#remove-image').removeClass('invisible');							
						} );
					} );
					
					
				}				
				    
				} );

        }
		
		function image_delete(id)  {		 
			$('#imgBox' + id).remove();
			$('#imageCount').val($('#imageCount').val() - 1);
			
			if ($('#imageCount').val() == 0)  {
			   $('#remove-image').addClass('invisible');	
			}			
		}		
    </script>
@endsection